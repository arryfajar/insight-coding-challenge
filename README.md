Insight Data Engineering - Coding Challenge
===========================================================
Arry Fajar Firdaus   
<uprising@gmail.com>  
<firdaus@usc.edu>

This repository contains solution to the Insight Data Engineering Coding Challenge. The directory structure of this repository follows the challenge specification. The src directory contains:

* average_degree.py: Main Python script that accepts Twitter input files and outputs average degree of the hashtag graph vertices.
* test_graph.py: Python unit test script that tests Graph class functionality in average_degree.py
* get-tweets.py: Twitter data generator script modified to print the data to the standard output instead of file.
* servicelog.py: Script that periodically print memory consumption of a process on Windows.

The insight_testsuite/tests directory includes all test cases used in the development to make sure that the solution conform to the challenge requirements.

# Requirements
The solution requires Python 2.7+ to run.

# Usage
run.sh handles the execution of the script as specified in the challenge rule.

average_degree.py accepts one or more text file input as argument and print average degree of hashtag graph for each valid line. The text file should contain one JSON Twitter message for each line. If no argument is given, the script will read from the standard input. This example shows how to run the script with tweets.txt as input and write the output to output.txt:

    python average_degree.py tweets.txt > output.txt

# Testing with Live Stream Data

This command print average degree from live Twitter stream using modified Twitter data generator script get-tweets.py:
    
    python get-tweets.py | python average_degree.py

After running the command for about half hour, it is shown that the average_degree.py script can keep up with live Twitter stream and the average memory consumption was about 22 MB (obtained using servicelog.py).
