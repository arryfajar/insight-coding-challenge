import fileinput
import json
from itertools import combinations
from collections import defaultdict
import pprint
import time
import sys
from heapq import *

DEBUG = 0
EXPIRY = 60 #seconds

class Graph:
    """An implementation of weighted graph where adding an existing edge
    will increase its weight. Removing an edge means reducing its weight
    and an edge will be completely removed from the graph if its weight 
    is zero.
    """

    def __init__(self):
        """Initialize a graph"""
        self.nodes = defaultdict(int)
        self.edges = defaultdict(int)

    def add_edge(self, node1, node2):
        """Add an edge to the graph. If the edge is already exist,
        add its weight.
        
        Args:
            node1: first node of the edge
            node2: second node of the edge
        """
        tup = tuple(sorted([node1, node2]))
        if tup not in self.edges:
            self.nodes[node1] += 1
            self.nodes[node2] += 1
        self.edges[tup] += 1

    def remove_edge(self, node1, node2):
        """Reduce an edge weight. If the weight becomes zero, remove the
        edge from the graph
        
        Args:
            node1: first node of the edge
            node2: second node of the edge
        """
        tup = tuple(sorted([node1, node2]))
        if tup not in self.edges:
            raise KeyError
        self.edges[tup] -= 1
        if self.edges[tup] == 0:
            del self.edges[tup]
            for node in tup:
                self.nodes[node] -= 1
                if self.nodes[node] == 0:
                    del self.nodes[node]

    def get_average_degree(self):
        """Return average degree of the graph nodes"""
        if len(self.nodes) == 0:
            return 0
        else:
            return sum(self.nodes.values())*1.0/len(self.nodes)

    def get_node_edges(self, node):
        """Return edges containing node `node`"""
        return [ x for x in self.edges.keys() 
                if x[0] == node or x[1] == node ]

def ts2str(ts):
    """Convert UNIX timestamp to string"""
    return datetime.datetime.fromtimestamp(ts).strftime('%H%M%S')

def process_tweets(tweets):
    graph = Graph()
    latest_timestamp = 0
    # Setup a min-heap containing hashtags for each tweet sorted by
    # timestamp.
    heap = []
    for line in tweets:
        # Parse tweet, skip on error
        try:
            tweet = json.loads(line)
            hashtags = [ tag['text'] for tag 
                    in tweet['entities']['hashtags'] ]
            timestamp = time.mktime(time.strptime(tweet['created_at'],
                    '%a %b %d %H:%M:%S +0000 %Y'))
        except (KeyError, ValueError):
            continue

        # Skip tweet that is out of order and outside time window
        if timestamp >= latest_timestamp-EXPIRY:
            if timestamp > latest_timestamp:
                latest_timestamp = timestamp

            # Remove expired tweet from the heap and its corresponding
            # edges
            while heap and heap[0][0] < latest_timestamp-EXPIRY:
                (ts, tag_combs) = heappop(heap)
                for comb in tag_combs:
                    graph.remove_edge(comb[0], comb[1])

            # Add tweet to the min heap and add each combination of hashtag 
            # pair to the graph
            if len(hashtags) >= 2:
                tag_combs = tuple(combinations(hashtags, 2))
                heappush(heap, (timestamp, tag_combs))
                for comb in tag_combs:
                    graph.add_edge(comb[0], comb[1])

        yield graph.get_average_degree()

    if DEBUG:
        print >> sys.stderr, "Num of edges:", len(graph.edges)
        print >> sys.stderr, "Num of nodes:", len(graph.nodes)
        print >> sys.stderr, "Sum of degree:", sum(graph.nodes.values())
        node = sorted(graph.nodes.items(),
                key=lambda x: x[1], reverse=True)[0]
        print >> sys.stderr, "Node with highest degree:", node
        print >> sys.stderr, "Edges of node with highest degree:", \
            graph.get_node_edges(node[0])
        print >> sys.stderr, "Average degree: ", graph.get_average_degree()
        pprint.pprint(graph.nodes, sys.stderr)

if __name__ == '__main__':
    try:
        inpf = fileinput.input()
        for avg_degree in process_tweets(inpf):
            # Truncate average degree to two decimal positions
            avgstr = '%.3f' % avg_degree
            print avgstr[:-1]
    except IOError as e:
        sys.stderr.write(
                "I/O error({0}): {1}\n".format(e.errno, e.strerror))


