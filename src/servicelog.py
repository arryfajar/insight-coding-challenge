import subprocess
import sys
import re
import time

while True:
    outputstr = subprocess.check_output(['tasklist', '/fi', 'PID eq %s' % sys.argv[1]])
    m = re.search("([\d,]+ K)\r", outputstr)
    if m:
        curtime = time.time()
        timestr = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(curtime))
        print "%s\t%s" % (timestr, m.group(1))
    time.sleep(300)
    