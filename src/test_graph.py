import unittest
from average_degree import Graph

class TestGraphMethods(unittest.TestCase):
    def setUp(self):
        self.g = Graph()

    def test_initial_average_degree(self):
        self.assertEqual(self.g.get_average_degree(), 0)

    def test_add_one_edge(self):
        self.g.add_edge("a", "b")
        self.assertEqual(self.g.get_average_degree(), 1)
        self.assertEqual(self.g.edges, {("a","b"): 1})
        self.assertEqual(self.g.nodes, {"a":1, "b":1})

    def test_add_multiple_edges(self):
        self.g.add_edge("a", "b")
        self.g.add_edge("b", "c")
        self.assertEqual(self.g.get_average_degree(), 4.0/3)
        self.assertEqual(self.g.edges,
            {
                ("a","b"): 1,
                ("b","c"): 1,
            })
        self.assertEqual(self.g.nodes, {"a":1, "b":2, "c": 1})

    def test_remove_edge(self):
        self.g.add_edge("a", "b")
        self.g.add_edge("b", "c")
        self.g.add_edge("c", "a")
        self.g.remove_edge("a", "b")
        self.assertEqual(self.g.get_average_degree(), 4.0/3)
        self.assertEqual(self.g.edges,
            {
                ("b","c"): 1,
                ("a","c"): 1,
            })
        self.assertEqual(self.g.nodes, {"a":1, "b":1, "c": 2})

    def test_add_weight(self):
        self.g.add_edge("a", "b")
        self.g.add_edge("b", "c")
        self.g.add_edge("c", "a")
        self.g.add_edge("a", "b")
        self.assertEqual(self.g.get_average_degree(), 2.0)
        self.assertEqual(self.g.edges,
            {
                ("b","c"): 1,
                ("a","c"): 1,
                ("a","b"): 2,
            })
        self.assertEqual(self.g.nodes, {"a":2, "b":2, "c": 2})

    def test_remove_weight(self):
        self.g.add_edge("a", "b")
        self.g.add_edge("b", "c")
        self.g.add_edge("c", "a")
        self.g.add_edge("a", "b")
        self.g.add_edge("c", "d")
        self.g.remove_edge("a", "b")
        self.assertEqual(self.g.get_average_degree(), 2.0)
        self.assertEqual(self.g.edges,
            {
                ("b","c"): 1,
                ("a","c"): 1,
                ("a","b"): 1,
                ("c","d"): 1,
            })
        self.assertEqual(self.g.nodes, {"a":2, "b":2, "c": 3, "d": 1})
if __name__ == '__main__':
    unittest.main()
